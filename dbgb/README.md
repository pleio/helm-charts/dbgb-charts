# dbgb

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for deploying dbgb

## Source Code

* <https://gitlab.com/pleio/helm-charts/dbgb-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.database.host | string | `nil` |  |
| config.database.name | string | `nil` |  |
| config.database.password | string | `nil` |  |
| config.database.user | string | `nil` |  |
| config.oauth.clientId | string | `nil` |  |
| config.oauth.clientSecret | string | `nil` |  |
| config.oauth.requestAccess | string | `nil` |  |
| config.oauth.url | string | `nil` |  |
| config.secretKey | string | `nil` |  |
| config.smtp.from | string | `nil` |  |
| config.smtp.host | string | `nil` |  |
| config.smtp.password | string | `nil` |  |
| config.smtp.port | string | `nil` |  |
| config.smtp.tls | string | `nil` |  |
| config.smtp.user | string | `nil` |  |
| domain | string | `nil` |  |
| env | string | `nil` |  |
| existingSecret | string | `nil` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/dbgb"` |  |
| image.tag | string | `nil` |  |
| ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt-pleio-prod-issuer"` |  |
| ingress.annotations."kubernetes.io/ingress.class" | string | `"nginx"` |  |
| replicaCount | int | `1` |  |
| storage.className | string | `"efs-v2"` |  |
| storage.existingDataStorage | string | `nil` |  |
| storage.storageSize | string | `"20Gi"` |  |
